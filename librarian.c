#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"library.h"



void librarian_area(user_t *u){
      int choice;
      char name[80];
	  char email[30];
	  char password[15];
	  int memberid;
do{
printf("\n\n0.sign out\n 1.Add Member\n 2.Edit Profile\n 3.Change Password\n 4.Add Book\n 5.Find Book\n 6.Edit Book\n 7.Check Availability\n 8.Add New Copy\n 9.Change rack\n 10.Issue Copy\n 11.Return Copy\n 12.Take Payment\n 13.Payement History\n 14.display all member \nEnter choice");
scanf("%d",&choice);
switch(choice)
   {
     case 1://add member
     add_member();
        break;
     case 2://edit profile
	 printf("Enter email");
         scanf("%s",email);
         printf("Enter password");
         scanf("%s",password);

          edit_profile(email,password);
        break;
     case 3://change password
     change_password();
        break;
     case 4://add book
         add_book();
        break;
     case 5:
         printf("Enter book name");
         scanf("%s",name);
         book_find_by_name(name);
        break;
     case 6://edit book
        book_edit_by_id();
        break;
     case 7://check availability
        bookcopy_checkavail_details();
        break;
     case 8: //Add copy
       bookcopy_add();
        break;
     case 9://change rack
         change_rack();
        break;
     case 10://issue book
      bookcopy_issue();
        break;
     case 11://return book copy
     bookcopy_return();
        break;
     case 12://take payment 
	    take_payment();
        break;
     case 13://Payment History
	 printf("enter member id of the member: ");
	scanf("%d", &memberid);
       payment_history(memberid);
        break;
	 case 14://display all member
	 display_all_members();
	   break;
    
    }
}while(choice !=0);

}

void add_member(){
   //input member details
   user_t u;
   user_accept(&u);
   //change user role to librarian
   user_add(&u);
}
 
 void add_book() {
	FILE *fp;
	// input book details
	book_t b;
	book_accept(&b);
	b.id = get_next_book_id();
	// add book into the books file
	// open books file.
	fp = fopen(BOOKS_DB, "ab");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	// append book to the file.
	fwrite(&b, sizeof(book_t), 1, fp);
	printf("book added in file.\n");
	// close books file.
	fclose(fp);
}

void book_edit_by_id(){
   book_t b;
   //input book id from user.
   int id,found=0;
   printf("enter book id :");
   scanf("%d",&id);

   //open books file
   FILE *fp;
   fp=fopen(BOOKS_DB,"rb+");
   if(fp==NULL){
      perror("cannot open books file");
      exit(1);
   }
   //read books one by one and check if book  with given id is found.
  while(fread(&b,sizeof(book_t),1,fp)>0){
     if(id == b.id){
        found=1;
        break;
     }
  }
   //if found,
   if(found){
   //input new book details from user
   book_t nb;
   long size=sizeof(book_t);
   book_accept(&nb);
   nb.id=b.id;
    //take find position one record behind.
  fseek(fp,-size,SEEK_CUR);
     //overwrite book details into the file
   fwrite(&nb,size,1,fp);
   printf("book updated.\n");
    }
   else
   {
 //if not found
 printf("Book not found.\n");
   //show message to user that book not found.
   }
   //close books file.
   fclose(fp);
}

void bookcopy_add() {
	FILE *fp;
	// input book copy details
	bookcopy_t b;
	bookcopy_accept(&b);
	b.id = get_next_bookcopy_id();
	// add book copy into the books file
	// open book copies file.
	fp = fopen(BOOKCOPY_DB, "ab");
	if(fp == NULL) {
		perror("cannot open book copies file");
		exit(1);
	}
	// append book to the file.
	fwrite(&b, sizeof(bookcopy_t), 1, fp);
	printf("book copy added in file.\n");
	// close books file.
	fclose(fp);
}

void bookcopy_checkavail_details(){
   int book_id;
   FILE *fp;
   bookcopy_t bc;
   int count=0;
   
   //input book id from the user
   printf("Enter the bookid :");
   scanf("%d",&book_id);

   //open book copies file
   fp=fopen(BOOKCOPY_DB,"rb");
    if(fp==NULL){
       perror("cannot open bookcopies file");
       return ;
    }
   //read bookcopy record one by one
    while(fread(&bc,sizeof(bookcopy_t),1,fp)>0){
       //if book id matching and status is available,print copy detail
       if(bc.bookid == book_id && strcmp(bc.status,STATUS_AVAIL)==0){
          bookcopy_display(&bc);
          count++;
       }
    }
   //if book id matching and status is available,print copy detail

   //close book copies file 
         fclose(fp);
   //if no copy is available,print the message.

if(count == 0)
printf("No copies aailable\n");

}

void bookcopy_issue() {
	issuerecord_t rec;
	FILE *fp;
	// accept issuerecord details from user
	issuerecord_accept(&rec);
	//TODO: if user is not paid, give error & return.

	if(!is_paid_member(rec.memberid)) {
		printf("member is not paid.\n");
		return;
	}
	// generate & assign new id for the issuerecord
	rec.id = get_next_issuerecord_id();
	// open issuerecord file
	fp = fopen(ISSUERECORD_DB, "ab");
	if(fp == NULL) {
		perror("issuerecord file cannot be opened");
		exit(1);
	}
	// append record into the file
	fwrite(&rec, sizeof(issuerecord_t), 1, fp);
	// close the file
	fclose(fp);

	// mark the copy as issued
	bookcopy_changestatus(rec.copyid, STATUS_ISSUED);
}

void bookcopy_changestatus(int bookcopy_id, char status[]) {
	bookcopy_t bc;
	FILE *fp;
	long size = sizeof(bookcopy_t);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open book copies file");
		return;
	}

	// read book copies one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if bookcopy id is matching
		if(bookcopy_id == bc.id) {
			// modify its status
			strcpy(bc.status, status);
			// go one record back
			fseek(fp, -size, SEEK_CUR);
			// overwrite the record into the file
			fwrite(&bc, sizeof(bookcopy_t), 1, fp);
			break;
		}
	}
	
	// close the file
	fclose(fp);
}

  void display_issued_bookcopies(int member_id) {
	FILE *fp;
	issuerecord_t rec;
	// open issue records file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
		perror("cannot open issue record file");
		
	}

	// read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// if member_id is matching and return date is 0, print it.
		if(rec.memberid == member_id && rec.return_date.day == 0)
			issuerecord_display(&rec);
	}  
	// close the file
	fclose(fp);
}

void bookcopy_return() {
	int member_id, record_id;
	FILE *fp;
	issuerecord_t rec;
	int diff_days, found = 0;
	long size = sizeof(issuerecord_t);
	// input member id
	printf("enter member id: ");
	scanf("%d", &member_id);
	// print all issued books (not returned yet)
	display_issued_bookcopies(member_id);
	// input issue record id to be returned.
	printf("enter issue record id (to return): ");
	scanf("%d", &record_id);
	// open issuerecord file
	fp = fopen(ISSUERECORD_DB, "rb+");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
	// read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// find issuerecord id
		if(record_id == rec.id) {
			found = 1;
			// initialize return date
			rec.return_date = date_current();
			// check for the fine amount
			diff_days = date_cmp(rec.return_date, rec.return_duedate);
			// update fine amount if any
			if(diff_days > 0)
				rec.fine_amount = diff_days * FINE_PER_DAY;
			break;
		}
	}
	
	if(found) {
		// go one record back
		fseek(fp, -size, SEEK_CUR);
		// overwrite the issue record
		fwrite(&rec, sizeof(issuerecord_t), 1, fp);
		// print updated issue record.
		printf("issue record updated after returning book:\n");
		issuerecord_display(&rec);
		// update copy status to available 
		bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
	}
	
	// close the file.
	fclose(fp);
}

void change_rack()
{
	int copy_id, found = 0;
	long int size = sizeof(bookcopy_t);
	bookcopy_t bookcopy;

	// open book copy file
	FILE *fp = fopen(BOOKCOPY_DB, "rb+");
	if (fp == NULL)
	{
		perror("failed to open bookcopy file");
		return;
	} 

	// accept copy id
	printf ("enter copy id : ");
	scanf("%d",&copy_id);

	// read book copy file 
	while (fread(&bookcopy, size, 1, fp)> 0)
	{
		//if copy id is found
		if(copy_id == bookcopy.id)
		{
			found = 1;
			printf("current rack : %d\n", bookcopy.rack);

			printf ("new rack : ");
			scanf("%d",&bookcopy.rack);

			bookcopy.id = copy_id;
			
	   strcpy(bookcopy.status, STATUS_AVAIL);
			break;
		}
	}
	// adjust  ptr position behind
	fseek(fp, -size , SEEK_CUR);
	// write into file
	fwrite(&bookcopy, size , 1,fp);
	fclose(fp);
	if (found == 0)
		printf("copy id not found");
	else 
		printf("copy updated in the new rack");
}

void display_all_members()
{
    //list all users
 user_t u;
    FILE * fptr = fopen(USER_DB,"rb");

    while ( fread(&u,sizeof(user_t),1,fptr) > 0 )
    {
        user_display(&u);
        //printf(" %s\n",u.password);
    }
    fclose(fptr);



}

int is_paid_member(int memberid)

 {
	date_t now = date_current();
	FILE *fp;
	payment_t pay;
	int paid = 0;
	// open file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp==NULL) {
		perror("cannot open payment file");
		return 0;
	}
	// read payment one by one till eof
	while(fread(&pay, sizeof(payment_t), 1, fp) > 0) {
		if(pay.memberid == memberid && pay.next_pay_duedate.day != 0
 && date_cmp(now, pay.next_pay_duedate) < 0) {
			paid = 1;
			break;
		}
	}
	// close file	
	fclose(fp);
	return paid;
}

void fine_payment_add(int memberid, double fine_amount) {
	FILE *fp;
	// initialize fine payment
	payment_t pay;
	pay.id = get_next_payment_id();
	pay.memberid = memberid;
	pay.amount = fine_amount;
	strcpy(pay.type, PAY_TYPE_FINE);
	pay.tx_time = date_current();
	memset(&pay.next_pay_duedate, 0, sizeof(date_t));
	// open the file
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open payment file");
		exit(1);
	}
	// append payment data at the end of file
	fwrite(&pay, sizeof(payment_t), 1, fp);
	// close the file
	fclose(fp);
}

