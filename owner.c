#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"library.h"


void owner_area(user_t *u){
    int choice;
    char email[30];
    char password[15];

do{
printf("\n\n0.sign out\n 1.Appoint Librarian\n 2.Edit Profile\n 3.Change Password\n 4.Fees/Fine Report\n 5.Boook Availability\n 6.Book Categories-subject\nEnter choice");
scanf("%d",&choice);
switch(choice)
   {
     case 1: //add librarian
    appoint_librarian();
        break;
     case 2://Edit profile
      printf("Enter email");
         scanf("%s",email);
         printf("Enter password");
         scanf("%s",password);

          edit_profile(email,password);
        break;
     case 3://change password
      change_password();
        break;
     case 4://Fees and Fine report
           fees_report();
        break;
     case 5://book availability
     bookcopy_checkavail_details();
        break;
     case 6://boook categories - subject
        book_category();
        break;
    
    }
}while(choice !=0);

}

void appoint_librarian() {
	// input librarian details
	user_t u;
	user_accept(&u);
	// change user role to librarian
	strcpy(u.role, ROLE_LIBRARIAN);
	// add librarian into the users file
	user_add(&u);
}

void book_category()
{
   book_t b;
    bookcopy_t c;
    FILE *f, *x;
    f=fopen(BOOKS_DB,"rb");
    if(f==NULL)
    {
        perror("File cannot be opened\n");
        return;
    }
    x=fopen(BOOKCOPY_DB,"rb");
    if(x==NULL)
    {
        perror("File cannot be opened\n");
        return;
    }
    while(fread(&b,sizeof(book_t),1,f)>0)
    {
        book_display(&b);
        while(fread(&c, sizeof(bookcopy_t),1,x)>0)
        {
            if(b.id==c.bookid)
            bookcopy_display(&c);
        }
    }
  fclose(f);
  fclose(x);
}

void fees_report()
{
    FILE *fp;
    payment_t p;
    fp=fopen(PAYMENT_DB,"rb");
    double fees=0;
    date_t d1,d2;
    printf("enter the intial date: \n");
    date_accept(&d1);
    printf("enter the final date: ");
    date_accept(&d2);
    if(fp == NULL) {
		perror("cannot open payment record file");
		exit(1);
	
    }
    while(fread(&p,sizeof(payment_t),1,fp)>0)
    {
        if(date_cmp(p.tx_time,d2)<=0)
        if(strcmp(p.type,PAY_TYPE_FEES)==0)
        fees+=p.amount;
    }
    fclose(fp);
    printf("from date : ");
    date_print(&d1);
    printf("to date : ");
    date_print(&d2);
    printf("Fees : %0.2lf\n",fees);
}